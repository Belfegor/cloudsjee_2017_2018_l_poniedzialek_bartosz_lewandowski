package com.bartosz.lewandowski.cloud.mapreduce;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<Object, Text, Text, DoubleWritable> {

    public void map(Object o, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] tuple = line.split("\\n");
        for (int i = 0; i < tuple.length; i++) {
            JsonObject obj = new JsonParser().parse(tuple[i]).getAsJsonObject();
            String side = obj.get("side").getAsString();
            String id = obj.get("series").getAsString();
            String sample = obj.get("sample").getAsString();

            double name = (obj.get("features2D")).getAsJsonObject().get("first").getAsDouble();
            String key = id + "_" + side + "_first" + " ";
            context.write(new Text(key), new DoubleWritable(name));

            name = (obj.get("features2D")).getAsJsonObject().get("second").getAsDouble();
            key = id + "_" + side + "_second" + " ";
            context.write(new Text(key), new DoubleWritable(name));

            name = (obj.get("features2D")).getAsJsonObject().get("third").getAsDouble();
            key = id + "_" + side + "_third" + " ";
            context.write(new Text(key), new DoubleWritable(name));

            name = (obj.get("features2D")).getAsJsonObject().get("fourth").getAsDouble();
            key = id + "_" + side + "_fourth" + " ";
            context.write(new Text(key), new DoubleWritable(name));

            name = (obj.get("features2D")).getAsJsonObject().get("fifth").getAsDouble();
            key = id + "_" + side + "_fifth" + " ";
            context.write(new Text(key), new DoubleWritable(name));

        }
    }
}
