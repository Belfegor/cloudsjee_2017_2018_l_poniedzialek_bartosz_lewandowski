package com.bartosz.lewandowski.cloud.mapreduce;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Math.sqrt;
import static java.lang.Math.pow;


public class MyReducer extends Reducer<Text, IntWritable, Text, DoubleWritable> {
    private DoubleWritable result = new DoubleWritable();

    public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        double sum = 0;
        double mean = 0;
        double sigma = 0;
        ArrayList<Double> values2 = new ArrayList<>();
        for (DoubleWritable val : values) {
            sum += val.get();
            values2.add(val.get());
        }
        mean = sum / 20.0;
        for(Double val : values2){
            sigma += pow(val - mean, 2.0);
        }
        sigma = sqrt(sigma/20.0);
        result.set(sigma);
        context.write(key, result);
    }

}
